package com.rickie.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author rickie
 * @version 1.0.0
 * @ClassName BookCargoCommand.java
 * @Description TODO
 * @createTime 2021年06月14日 00:46:00
 */
@Data
public class CargoBookCmd {
    private String bookingId;
    private int bookingAmount;
    private String originLocation;
    private String destLocation;
    private Date destArrivalDeadline;
}
